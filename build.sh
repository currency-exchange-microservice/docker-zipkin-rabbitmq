#!/bin/sh

# Docker setup
docker network create spring
docker run -d --network spring --name rabbit --hostname my-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management
sleep 5 # wait for rabbit to startup
docker run -d --network spring --name zipkin -p 9411:9411 -e RABBIT_URI="amqp://rabbit" openzipkin/zipkin